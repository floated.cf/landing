class SyncService {
  constructor ($timeout, $rootScope, $docker, $nginx) {
    this.$timeout = $timeout
    this.$rootScope = $rootScope
    this.$docker = $docker
    this.$nginx = $nginx

    // URL
    this._URL = 'ws://163.172.178.167:8099'
    this._PENDING = {}
    this.ATTEMPT = 1

    this.CONNECTED = false
    this.LOGGED = false

    this.handle = payload => {
      this.$docker.setContainers(JSON.parse(payload.docker))
      this.$nginx.setSites(JSON.parse(payload.nginx))
    }

    this.connect()
    // console.log('$sync loaded')
  }

  // WebSocket operations
  init () {
    this._AUTO = true

    this.ws.onopen = () => {
      console.log(`❓ Socket with ${this._URL}: OPEN`)
      this.CONNECTED = true
      this.ATTEMPT = 1
    }

    this.ws.onmessage = message => {
      let messageObject = JSON.parse(message.data)
      if (messageObject && messageObject.docker && messageObject.nginx) {
        this.$rootScope.$apply(() => this.handle(messageObject))
      }
    }

    this.ws.onclose = () => {
      console.log(`✖️ Socket with ${this._URL}: CLOSED`)
      this.CONNECTED = false
      this.LOGGED = false
      if (this._AUTO) {
        this.reconnect()
      }
    }
  }
  // Connection
  connect () {
    this.ws = new WebSocket(this._URL)
    this.init()
  }
  disconnect () {
    this._AUTO = false
    this.ws.terminate()
  }
  reconnect () {
    console.log(`🤔 Attempting to reconnect in ${this.ATTEMPT * 2.5} seconds...`)
    this.$timeout(() => {
      this.connect()
    }, this.ATTEMPT * 2500)
    this.ATTEMPT += this.ATTEMPT
  }
  // Update service
  update (message) {
    this.ws.send(message)
  }
  // Send
  do (action, payload = {}) {
    let resend = setInterval(() => {
      if (this.ws.readyState === 1) {
        this.ws.send(JSON.stringify({action, payload}))
        clearInterval(resend)
      }
    }, 100)
  }
}

SyncService.$inject = ['$timeout', '$rootScope', '$docker', '$nginx']

export default SyncService
