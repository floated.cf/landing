class Nginx {
  constructor () {
    this.sites = []
    this.updated = new Date().toString()
  }

  $onInit () {

  }

  getSites () {
    return this.sites
  }

  setSites (sites) {
    this.sites = sites
    this.updated = new Date().toString()
  }
}

Nginx.$inject = []

export default Nginx
