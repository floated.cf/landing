import angular from 'angular'

import syncService from './sync.service'
import dockerService from './docker.service'
import nginxService from './nginx.service'

export default angular.module('landing.services', [])
  .service('$sync', syncService)
  .service('$docker', dockerService)
  .service('$nginx', nginxService)
  .name
