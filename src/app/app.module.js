// import 'bootstrap/dist/css/bootstrap.css'
import '../style/nightstrap.css'
// import '../style/superhero.css'
import '../style/app.css'

import angular from 'angular'
import uiRouter from '@uirouter/angularjs'

import Common from './common/common.module'
import Components from './components/components.module'
import Services from './services/services.module'
import AppComponent from './app.component'

document.addEventListener('DOMContentLoaded', () => angular.bootstrap(document, ['landing']))

export default angular.module('landing', [
  Components,
  Common,
  Services,
  uiRouter
])
  .component('landingApp', AppComponent)
  .name
