import controller from './landing.controller'

export default {
  template: require('./landing.html'),
  controller
}
