import controller from './me.controller'

export default {
  template: require('./me.html'),
  controller
}
