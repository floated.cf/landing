class MeController {
  constructor ($state) {
    this.$state = $state
  }
}

MeController.$inject = ['$state']

export default MeController
